// --unsafe-perm=true --allow-root
const {Builder, By, until} = require('selenium-webdriver');
const { describe, it, after, before } = require('selenium-webdriver/testing');

(async function example() {
    try {
      describe ('library app scenarios', () => {
        beforeEach (() => {
          const driver = new Builder()
            .forBrowser('chrome')
            .build();
          await driver.get('https://library-app.firebaseapp.com/');
        });

        afterEach (() => {
          driver.quit();
        });

        it ('test 1', () => {

        });

        it ('test 2', () => {
          
        });

        it ('test 3', () => {
          
        });
      });


      
      const input = await driver.findElement(By.css('input'));
      await input.sendKeys('user@mail.com');
      const submitButton = await driver.findElement(By.css('.btn-lg'));

      driver.wait(async function () {
        // return submitButton.isEnabled();
        const opacityValue = await submitButton.getCssValue('opacity');
        return opacityValue === 1;
      }, 10000);

      submitButton.click();

      await driver.wait(until.elementLocated(By.css('.alert-success')), 10000, 'Looking for alert success.');
      const alertSuccess = await driver.findElement(By.css('.alert-success')).getText();
      console.log ('Alert Success: ' + alertSuccess);
    } catch (ex) {
      console.log ('Error: ' + ex.message);
    } finally {
      // console.log ('Closing driver.');
      // await driver.quit();
    }
  })();