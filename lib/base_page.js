const {Builder, By, until} = require('selenium-webdriver');
var fake = require('../utils/fake_data');

const chrome = require('selenium-webdriver/chrome');
let o = new chrome.Options();
o.addArguments('start-fullscreen');
o.addArguments('disable-infobars');
o.addArguments('headless');
o.setUserPreferences({ credential_enable_service: false });

var Page = function() {
    this.driver = new Builder()
        .setChromeOptions(o)
        .forBrowser('chrome')
        .build();
    this.fake = fake;
    
    this.visit = async function(theUrl) {
        return await this.driver.get(theUrl);
    }
    this.quit = async function() {
        return await this.driver.quit();   
    }
    this.find = async function(el) {
        await this.driver.wait(until.elementLocated(By.css(el)), 15000, 'Looking for element');
        const webElement = await this.driver.findElement(By.css(el));
        return webElement;
    }
    this.findLink = async function(linkText) {
        await this.driver.wait(until.elementLocated(By.linkText(linkText)), 15000);
        const webElement = await this.driver.findElement(By.linkText(linkText));
        return webElement;
    }
    this.findAll = async function(el) {
        await this.driver.wait(until.elementLocated(By.css(el)), 15000, 'Looking for elements');
        return await this.driver.findElements(By.css(el));
    }
    this.write = async function (el, txt) {
        return await el.sendKeys(txt);
    }
};

module.exports = Page;