let Page = require('./base_page');
const locator = require('../utils/locator');

const emailInput = locator.emailInput;
const requestInviteButton = locator.requestInviteBtn;
const alertSuccessTooltip = locator.alertSuccess;

Page.prototype.requestBtn = async function () {
    const fakeEmail = this.fake().email;
    const input = await this.find(emailInput);
    await this.write(input, fakeEmail);
    const submitButton = await this.find(requestInviteButton);
    const result = await this.driver.wait(async function () {
        const opacityValue = await submitButton.getCssValue('opacity');
        const buttonEnabledFlag = await submitButton.isEnabled();
        return {
            opacity: opacityValue,
            state: buttonEnabledFlag
        }
    }, 5000);
    return result;
};

Page.prototype.clickSubmit = async function () {
    return await this.find(requestInviteButton).clcik();
};

Page.prototype.alertSuccess = async function () {
    const fakeEmail = this.fake().email;
    const input = await this.find(emailInput);
    await this.write(input, fakeEmail);
    const submitButton = await this.find(requestInviteButton);
    submitButton.click();
    await this.driver.wait(this.find(alertSuccessTooltip), 10000, 'Looking for alert success.');
    const alertSuccess = await this.find(alertSuccessTooltip)
    const result = await this.driver.wait(async function() {
        const alertSuccessText = await alertSuccess.getText();
        return alertSuccessText;
    });
    return result;
};

module.exports = Page;