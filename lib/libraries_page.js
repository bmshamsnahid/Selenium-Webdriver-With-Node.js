let Page = require('./base_page');
const selector = require('../utils/locator');

const libraryItemSelector = selector.libraryItem;
const libraryNameInputSelector = selector.nameInput;
const libraryAddressInputSelector = selector.addressInput;
const libraryPhoneInputSelector = selector.phoneInput;
const createButtonSelector = selector.createBtn;
const liraryContainerSelector = selector.libraryContainer;
const inputFilterSelector = selector.inputFilter;

// list libraries
Page.prototype.listLibraries = async function () {
    const libraryItems = await this.findAll(libraryItemSelector);
    return libraryItems;
};

// add libraries
Page.prototype.addLibrary = async function (libraryName) {
    // generate fake new library info
    const fakeLibraryName = libraryName || await this.fake().libraryName;
    const fakeLibraryAddress = await this.fake().address;
    const fakeLibraryPhone = await this.fake().phone;
    
    // click on 'add new' to get the form input
    const addNewLink = await this.findLink('Add new');
    await addNewLink.click();
    
    // grab the new library input section as web element
    const libraryNameInput = await this.find(libraryNameInputSelector);
    const libraryAddressInput = await this.find(libraryAddressInputSelector);
    const libraryPhoneInput = await this.find(libraryPhoneInputSelector);
    
    // put the fake input data to the web element
    await this.write(libraryNameInput, fakeLibraryName);
    await this.write(libraryAddressInput, fakeLibraryAddress);
    await this.write(libraryPhoneInput, fakeLibraryPhone);

    // find the submit button and click submit
    const createButton = await this.find(createButtonSelector);
    await createButton.click();

    // get the final library list
    const libraryList = await this.find(liraryContainerSelector);
    const result = await this.driver.wait(async function () {
        const libraryListText = await libraryList.getText();
        return {
            libraryListText: libraryListText,
            libraryName: fakeLibraryName
        }
    }, 5000);

    return result;
};

// sort libraries
Page.prototype.filterLibraries = async function () {
    const newLibraryName = await this.fake().libraryName;
    await this.addLibrary(newLibraryName);
    const inputFilter = await this.find(inputFilterSelector);
    await this.write(inputFilter, newLibraryName);
    
    // get the filtered library list
    const libraryList = await this.find(liraryContainerSelector);
    const result = await this.driver.wait(async function () {
        const libraryListText = await libraryList.getText();
        return {
            libraryListText: libraryListText,
            libraryName: newLibraryName
        }
    }, 5000);

    return result;
};

module.exports = Page;