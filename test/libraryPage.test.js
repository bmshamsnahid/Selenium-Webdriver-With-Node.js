// --unsafe-perm=true --allow-root
const { describe, it, after, before } = require('mocha');
const Page = require('../lib/libraries_page');
let page;
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const should = chai.should();
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function example() {
    try {
        describe ('library app scenarios: Library listings', async function () {
            this.timeout(50000);
            let driver;
            beforeEach (async () => {
                page = new Page();
                driver = page.driver;
                page.visit('https://library-app.firebaseapp.com/libraries');
            });
    
            afterEach (async () => {
              await page.quit();
            });
    
            it ('Should list libraries', async () => {
                const libraries = await page.listLibraries();
                libraries.should.have.length.above(10);
            });
    
            it ('Should be able to add a new library', async () => {
                const addLibray = await page.addLibrary();
                addLibray.libraryListText.should.contain(addLibray.libraryName); 
            });
    
            it ('Should be able to filter libraries', async () => {
                const filterLibrarys = await page.filterLibraries();
                filterLibrarys.libraryListText.should.contain(filterLibrarys.libraryName);
            });
          });
    } catch (ex) {

    } finally {

    }
  })();