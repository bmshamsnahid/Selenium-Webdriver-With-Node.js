// --unsafe-perm=true --allow-root
const { describe, it, after, before } = require('mocha');
const Page = require('../lib/home_page');
let page;
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const should = chai.should();
chai.use(chaiAsPromised);

process.on('unhandledRejection', () => {});

(async function example() {
    try {
        describe ('library app scenarios: Home page', async function () {
            this.timeout(50000);
            let driver;
            beforeEach (async () => {
                page = new Page();
                driver = page.driver;
                page.visit('https://library-app.firebaseapp.com');
            });
    
            afterEach (async () => {
              await page.quit();
            });
    
            it ('Typing valid email changes button opacity to 1', async () => {
                const result = await page.requestBtn();
                result.opacity.should.equal('1');
            });
    
            it ('Typing a valid email enables request button', async () => {
                const result = await page.requestBtn();
                result.state.should.be.true;
            });
    
            it ('Clicking request invitation triggers a confirmation alert', async () => {
                 const result = await page.alertSuccess();
                 result.should.contain('Thank you! We saved your email address with the following id:');
            });
          });
    } catch (ex) {

    } finally {

    }
  })();