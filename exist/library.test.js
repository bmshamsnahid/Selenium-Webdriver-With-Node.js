// --unsafe-perm=true --allow-root
const {Builder, By, until} = require('selenium-webdriver');
const { describe, it, after, before } = require('mocha');
const assert = require('assert');
// var assert = require('chai').assert;

process.on('unhandledRejection', () => {});

(async function example() {
    try {
        describe ('library app scenarios', async function () {
            this.timeout(50000);
            let driver;
            beforeEach (async () => {
                driver = new Builder()
                    .forBrowser('chrome')
                    .build();
                await driver.get('https://library-app.firebaseapp.com/');
            });
    
            afterEach (async () => {
              await driver.quit();
            });
    
            it ('Changes button opacity upon email being filled out', async () => {
                const input = await driver.findElement(By.css('input'));
                await input.sendKeys('user@mail.com');
                const submitButton = await driver.findElement(By.css('.btn-lg'));
                driver.wait(async function () {
                  const opacityValue = await submitButton.getCssValue('opacity');
                  console.log ('Value of opacity: ' + opacityValue);
                  // await assert (opacityValue, '1', 'Opacity value should be 1');
                    return assert.equal(opacityValue, '2');
                }, 5000);
            });
    
            it ('Got the alert success message', async () => {
                const input = await driver.findElement(By.css('input'));
                await input.sendKeys('user@mail.com');
                const submitButton = await driver.findElement(By.css('.btn-lg'));
                submitButton.click();
                await driver.wait(until.elementLocated(By.css('.alert-success')), 10000, 'Looking for alert success.');
                const alertSuccess = await driver.findElement(By.css('.alert-success')).getText();
            });
    
            it ('Got navbar items', async () => {
                const navbarText = await driver.findElement(By.css('nav'));
            });
          });
    } catch (ex) {

    } finally {

    }
  })();